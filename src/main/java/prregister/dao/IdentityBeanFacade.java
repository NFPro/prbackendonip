/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.dao;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import prregister.beans.IdentityBean;
import prregister.beans.PersonBean;

/**
 *
 * @author Fiston Songa
 */
@Stateless
public class IdentityBeanFacade extends AbstractFacade<IdentityBean>
{
    @EJB
    private PersonBeanFacade pbf;

    @PersistenceContext(unitName = "onip-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }

    @Override
    public void edit(IdentityBean entity)
    {
        em.refresh(entity);
    } 

    public IdentityBeanFacade()
    {
        super(IdentityBean.class);
    }
    
    public List<IdentityBean> getIdentitiesOfPerson(String idPerson)
    {
        List<IdentityBean> identityBeans;
        identityBeans = (List<IdentityBean>) em.createQuery("select identity from IdentityBean identity where identity.person.personID = :idPerson")
                .setParameter("idPerson", idPerson)
                .getResultList();
        return identityBeans.size() > 0 ? identityBeans: null;
    }

    public IdentityBean findAnIdentityOfAPerson(String personId, String identityId)
    {
       IdentityBean identityBean;
        identityBean = (IdentityBean) em.createQuery("select identity.person  from IdentityBean identity where identity.person.personID = :personId and identity.identityID = :identityId ")
                .setParameter("personID", personId)
                .setParameter("identityId", identityId)
                .getSingleResult();
        return identityBean != null?  identityBean: null;
    }
    
}
