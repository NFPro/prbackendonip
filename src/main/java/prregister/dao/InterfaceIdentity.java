/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.dao;

import java.util.List;
import prregister.beans.IdentityBean;
import prregister.beans.PersonBean;

/**
 *
 * @author Fiston Songa
 */
public interface InterfaceIdentity
{
    List<IdentityBean> findByPerson(Long id);
}
