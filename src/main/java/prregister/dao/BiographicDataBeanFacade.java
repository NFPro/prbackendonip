/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import prregister.beans.BiographicDataBean;

/**
 *
 * @author Fiston Songa
 */
@Stateless
public class BiographicDataBeanFacade extends AbstractFacade<BiographicDataBean>
{
//
    @PersistenceContext(unitName = "onip-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }

    public BiographicDataBeanFacade()
    {
        super(BiographicDataBean.class);
    }
    
}
