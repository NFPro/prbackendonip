/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.rest;

import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Fiston Songa
 */
@Path("identity")
public class IdentityREST
{

    @GET
    @Path("persons/{personId}/identities")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response readAllbyPerson(@PathParam("personId") String personId)
    {
        return null;//to do
    }

    @POST
    @Path("persons/{personId}/identities")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createOneIdentity(@PathParam("personId") String personId)
    {
        return null;//to do
    }

    @POST
    @Path("persons/{personId}/identities/{identityId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createIdentityForPerson(@PathParam("personId") String personId, @PathParam("identityId") String identityId)
    {
        return null;//to do
    }
    
    @GET
    @Path("persons/{personId}/identities/{identityId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response readOneIdentity(@PathParam("personId") String personId, @PathParam("identityId") String identityId)
    {
        return null;//to do
    }
    
    @PUT
    @Path("persons/{personId}/identities/{identityId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateOneIdentity(@PathParam("personId") String personId, @PathParam("identityId") String identityId)
    {
        return null;//to do
    }
    
    @DELETE
    @Path("persons/{personId}/identities/{identityId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteIdentity(@PathParam("personId") String personId, @PathParam("identityId") String identityId)
    {
        return null;//to do
    }
    
    @PUT
    @Path("persons/{personId}/identities/{identityId}/status")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changeStatusOfIdentity(@PathParam("personId") String personId, @PathParam("identityId") String identityId)
    {
        return null;//to do
    }
}
