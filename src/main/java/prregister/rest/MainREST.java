/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.rest;

import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import prregister.beans.IdentityBean;
import prregister.beans.PersonBean;
import prregister.dao.IdentityBeanFacade;
import prregister.dao.PersonBeanFacade;
import prregister.enumerations.IdentityStatus;
import prregister.utils.CustomResponse;

@Path("persons")
public class MainREST
{

    @EJB
    private PersonBeanFacade personFacade;
    @EJB
    private IdentityBeanFacade identityFacade;

    @POST
    @Path("{personId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPerson(@PathParam("personId") String personId, PersonBean personBean)
    {
        if (personId == null || personBean == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "personId or personBean cannot be null")).build();
        }
        personBean.setPersonID(personId);
        try
        {
            personFacade.create(personBean);
            return Response.status(Response.Status.CREATED).entity(personBean).build();
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
        }
    }

    @GET
    @Path("{personId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response readPerson(@PathParam("personId") String personId)
    {
        if (personId == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "personId cannot be null")).build();
        }
        PersonBean personBean;
        try
        {
            personBean = personFacade.find(personId);
            return Response.status(Response.Status.OK).entity(personBean).build();
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
        }
    }

    @PUT
    @Path("{personId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatePerson(@PathParam("personId") String personId, PersonBean personBean)
    {
        if (personId == null || personBean == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "personId or personBean cannot be null")).build();
        }

        PersonBean found;
        try
        {
            found = personFacade.find(personId);
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
        }

        if (found != null)
        {
            found.setStatus(personBean.getStatus());
            found.setPhysicalStatus(personBean.getPhysicalStatus());

            try
            {
                personFacade.edit(found);
                return Response.status(Response.Status.NO_CONTENT).entity(new CustomResponse("Success: ", "Update successful")).build();
            }
            catch (Exception e)
            {
                return Response.status(Response.Status.FORBIDDEN).entity(new CustomResponse("Error: ", e.getMessage())).build();
            }
        }
        else
        {
            return Response.status(Response.Status.FORBIDDEN).entity(new CustomResponse("Error: ", "Chauffeur not found")).build();
        }
    }

    @DELETE
    @Path("{personId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deletePerson(@PathParam("personId") String personId)
    {
        if (personId == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "personId cannot be null")).build();
        }

        PersonBean found;

        try
        {
            found = personFacade.find(personId);
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
        }

        if (found != null)
        {
            try
            {
                personFacade.remove(found);
                return Response.status(Response.Status.NO_CONTENT).build();
            }
            catch (Exception e)
            {
                return Response.status(Response.Status.FORBIDDEN).entity(new CustomResponse("Error: ", e.getMessage())).build();
            }
        }
        else
        {
            return Response.status(Response.Status.FORBIDDEN).entity(new CustomResponse("Error: ", "Person not found")).build();
        }
    }

    //IDENTITY SERVICES 
    @GET
    @Path("{personId}/identities")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response readAllbyPerson(@PathParam("personId") String personId)
    {
        if (personId == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "personId cannot be null")).build();
        }
        PersonBean bean;

        try
        {
            bean = personFacade.find(personId);
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
        }

        if (bean != null)
        {
            List<IdentityBean> identityBean;
            try
            {
                identityBean = identityFacade.getIdentitiesOfPerson(personId);
                return Response.ok(identityBean).build();
            }
            catch (Exception e)
            {
                return Response.status(Response.Status.FORBIDDEN).entity(new CustomResponse("Error: ", e.getMessage())).build();
            }
        }
        else
        {
            return Response.status(Response.Status.NOT_FOUND).entity(new CustomResponse("Error: ", "Person not exists")).build();
        }
    }

    @POST
    @Path("{personId}/identities")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createOneIdentity(@PathParam("personId") String personId, IdentityBean identityBean)
    {
        return null;//to do
    }

    @POST
    @Path("{personId}/identities/{identityId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createIdentityForKnowPerson(@PathParam("personId") String personId, @PathParam("identityId") String identityId, IdentityBean identityBean)
    {
        if (personId == null || identityId == null || identityBean == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "Parameters or Object cannot be null")).build();
        }
        PersonBean found;

        try
        {
            found = personFacade.find(personId);
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
        }

        if (found != null)
        {
            identityBean.setIdentityID(identityId);
            identityBean.setPerson(found);

            try
            {
                identityFacade.create(identityBean);
                return Response.status(Response.Status.CREATED).build();
            }
            catch (Exception e)
            {
                return Response.status(Response.Status.FORBIDDEN).entity(new CustomResponse("Error: ", e.getMessage())).build();
            }
        }
        else
        {
            return Response.status(Response.Status.NOT_FOUND).entity(new CustomResponse("Error: ", "Person not exists")).build();
        }
    }

    @GET
    @Path("{personId}/identities/{identityId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response readOneIdentity(@PathParam("personId") String personId, @PathParam("identityId") String identityId)
    {
        if (personId == null || identityId == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "personId or identityId cannot be null")).build();
        }
        boolean personExists = checkIfPersonIDExists(personId);
        boolean identityExits = checkIfIdentityIDExists(identityId);
        
        if (! personExists || ! identityExits)
        {
            return Response.status(Response.Status.NOT_FOUND).entity(new CustomResponse("Error: ", "Person or identity not exists")).build();
        }
        
        IdentityBean identityBean;
        try
        {
            identityBean = identityFacade.findAnIdentityOfAPerson(personId, identityId);
            
            return Response.status(Response.Status.OK).entity(identityBean).build();
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.FORBIDDEN).entity(new CustomResponse("Error: ", e.getMessage())).build();
        }    
    }

    @PUT
    @Path("{personId}/identities/{identityId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateOneIdentity(@PathParam("personId") String personId, @PathParam("identityId") String identityId, IdentityBean identityBean)
    {
        if (personId == null || identityId == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "personId or identityId cannot be null")).build();
        }
        boolean personExists = checkIfPersonIDExists(personId);
        boolean identityExits = checkIfIdentityIDExists(identityId);
        
        if (! personExists || ! identityExits)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", "Person or identity not exists")).build();
        }
        IdentityBean found;
        try
        {
            found = identityFacade.findAnIdentityOfAPerson(personId, identityId);
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
        } 
        
        if (found != null)
        {
            found.setBiographicDataBean(identityBean.getBiographicDataBean());
            found.setClientData(identityBean.getClientData());
            found.setIstatus(identityBean.getIstatus());
            found.setPerson(identityBean.getPerson());
            
            try
            {
                identityFacade.edit(found);
                return Response.status(Response.Status.NO_CONTENT).entity(new CustomResponse("Error: ", "Update successful")).build();
            }
            catch (Exception e)
            {
                return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
            }
        }
        else
        {
            return Response.status(Response.Status.NOT_FOUND).entity(new CustomResponse("Error: ", "Identity not found")).build();
        }     
    }

    @DELETE
    @Path("{personId}/identities/{identityId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteIdentity(@PathParam("personId") String personId, @PathParam("identityId") String identityId)
    {
         if (personId == null || identityId == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "personId or identityId cannot be null")).build();
        }
        boolean personExists = checkIfPersonIDExists(personId);
        boolean identityExits = checkIfIdentityIDExists(identityId);
        
        if (! personExists || ! identityExits)
        {
            return Response.status(Response.Status.NOT_FOUND).entity(new CustomResponse("Error: ", "Person or identity not exists")).build();
        }
        IdentityBean found;
        try
        {
            found = identityFacade.find(identityId);
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
        }
        
        if (found != null)
        {
            try
            {
                identityFacade.remove(found);
                return Response.status(Response.Status.NO_CONTENT).entity(new CustomResponse("Error: ", "Delete successful")).build();
            }
            catch (Exception e)
            {
                return Response.status(Response.Status.FORBIDDEN).entity(new CustomResponse("Error: ", "Delete not allowed")).build();
            }
        }
        else
        {
            return Response.status(Response.Status.NOT_FOUND).entity(new CustomResponse("Error: ", "Unknown record")).build();
        }
    }

    @PUT
    @Path("{personId}/identities/{identityId}/status")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changeStatusOfIdentity(@PathParam("personId") String personId, @PathParam("identityId") String identityId, @QueryParam("status") IdentityStatus status)
    {
         if (personId == null || identityId == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(new CustomResponse("Error: ", "personId or identityId cannot be null")).build();
        }
        boolean personExists = checkIfPersonIDExists(personId);
        boolean identityExits = checkIfIdentityIDExists(identityId);
        
        if (! personExists || ! identityExits)
        {
            return Response.status(Response.Status.NOT_FOUND).entity(new CustomResponse("Error: ", "Person or identity not exists")).build();
        }
        IdentityBean  found;
        
        try
        {
            found = identityFacade.find(identityId);
        }
        catch (Exception e)
        {
            return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
        }
        
        if (found != null)
        {
            found.setIstatus(status);
            try
            {
                identityFacade.edit(found);
                return Response.status(Response.Status.NO_CONTENT).build();
            }
            catch (Exception e)
            {
                return Response.status(Response.Status.CONFLICT).entity(new CustomResponse("Error: ", e.getMessage())).build();
            }
        }
        else
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }     
    }
    
    
    
    
    private boolean checkIfIdentityIDExists(String identityID)
    {
         if (identityID == null)
        {
            return false;
        }
        IdentityBean identityBean = null;      
        try
        {
            identityBean = identityFacade.find(identityID);
        }
        catch (Exception e)
        {
        }
        
        return identityBean != null ? true: false;
    }
    
    private boolean checkIfPersonIDExists(String personID)
    {
        PersonBean personBean = null;      
        try
        {
            personBean = personFacade.find(personID);
        }
        catch (Exception e)
        {
        }
        
        return personBean != null ? true: false;
    }
}
