
package prregister.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import prregister.enumerations.IdentityStatus;

@Entity
@Table(name = "identity")
public class IdentityBean implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "identityid")
    private String identityID;

    @Enumerated(EnumType.STRING)
    private IdentityStatus istatus;

    private byte[] clientData;

    @ManyToOne
    @JoinColumn(name = "personID" )
    private PersonBean person;

    @OneToOne
    @JoinColumn(name = "idbiography")
    private BiographicDataBean biographicDataBean;
    
    @OneToOne
    @JoinColumn(name = "idcontext")
    private ContextualDataBean contextualDataBean;
    
    @OneToMany
    @JoinColumn(name = "documentID")
    private List<DocumentBean> documents;

    public String getIdentityID()
    {
        return identityID;
    }

    public void setIdentityID(String identityID)
    {
        this.identityID = identityID;
    }

    public IdentityStatus getIstatus()
    {
        return istatus;
    }

    public void setIstatus(IdentityStatus istatus)
    {
        this.istatus = istatus;
    }

    public byte[] getClientData()
    {
        return clientData;
    }

    public void setClientData(byte[] clientData)
    {
        this.clientData = clientData;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (identityID != null ? identityID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IdentityBean))
        {
            return false;
        }
        IdentityBean other = (IdentityBean) object;
        if ((this.identityID == null && other.identityID != null) || (this.identityID != null && !this.identityID.equals(other.identityID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "onip.entities.Identity[ id=" + identityID + " ]";
    }

    
//
//    public PersonBean getPersonBean()
//    {
//        return personBean;
//    }
//
//    public void setPersonBean(PersonBean personBean)
//    {
//        this.personBean = personBean;
//    }
//
//    

    public PersonBean getPerson()
    {
        return person;
    }

    public void setPerson(PersonBean person)
    {
        this.person = person;
    }
    
    public BiographicDataBean getBiographicDataBean()
    {
        return biographicDataBean;
    }

    public void setBiographicDataBean(BiographicDataBean biographicDataBean)
    {
        this.biographicDataBean = biographicDataBean;
    }

    public List<DocumentBean> getDocuments()
    {
        return documents;
    }

    public void setDocuments(List<DocumentBean> documents)
    {
        this.documents = documents;
    }

    public ContextualDataBean getContextualDataBean()
    {
        return contextualDataBean;
    }

    public void setContextualDataBean(ContextualDataBean contextualDataBean)
    {
        this.contextualDataBean = contextualDataBean;
    }
    
    

}
