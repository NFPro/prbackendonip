
package prregister.beans;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name = "biography")
public class BiographicDataBean implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idbiography;
    
    @Column(length = 30, nullable = false)
    private String firstName;
    
    @Column(length = 30, nullable = false)
    private String lastname;
    
    @Column(length = 30, nullable = false)
    private String nationality;
    
    private char gender;

    private LocalDate dateOfBirthday;

    private LocalDate dateOfBDead;
    @Embedded
    private AddressBean address;
    
    @Embedded
    private OriginBean originBean;
    
    @OneToOne(mappedBy = "biographicDataBean")
    private IdentityBean identityBean;
    
    


    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public LocalDate getDateOfBirthday()
    {
        return dateOfBirthday;
    }

    public void setDateOfBirthday(LocalDate dateOfBirthday)
    {
        this.dateOfBirthday = dateOfBirthday;
    }

    public LocalDate getDateOfBDead()
    {
        return dateOfBDead;
    }

    public void setDateOfBDead(LocalDate dateOfBDead)
    {
        this.dateOfBDead = dateOfBDead;
    }

    public AddressBean getAddress()
    {
        return address;
    }

    public void setAddress(AddressBean address)
    {
        this.address = address;
    }
    
    

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idbiography != null ? idbiography.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the idbiography fields are not set
        if (!(object instanceof BiographicDataBean))
        {
            return false;
        }
        BiographicDataBean other = (BiographicDataBean) object;
        if ((this.idbiography == null && other.idbiography != null) || (this.idbiography != null && !this.idbiography.equals(other.idbiography)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "onip.entities.BiographicData[ id=" + idbiography + " ]";
    }

    public Long getIdbiography()
    {
        return idbiography;
    }

    public void setIdbiography(Long idbiography)
    {
        this.idbiography = idbiography;
    }

    public OriginBean getOriginBean()
    {
        return originBean;
    }

    public void setOriginBean(OriginBean originBean)
    {
        this.originBean = originBean;
    }

    public IdentityBean getIdentityBean()
    {
        return identityBean;
    }

    public void setIdentityBean(IdentityBean identityBean)
    {
        this.identityBean = identityBean;
    }

    public String getNationality()
    {
        return nationality;
    }

    public void setNationality(String nationality)
    {
        this.nationality = nationality;
    }

    public char getGender()
    {
        return gender;
    }

    public void setGender(char gender)
    {
        this.gender = gender;
    }
    
    
    
}
