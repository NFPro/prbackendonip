/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.beans;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Fiston Songa
 */
@Entity
@Table(name = "contextualdata")
public class ContextualDataBean implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idcontext;
    
    private LocalDate enrolmentdate;
    
    @OneToOne(mappedBy = "contextualDataBean")
    private IdentityBean identityBean;

    public Long getIdContext()
    {
        return idcontext;
    }

    public void setIdContext(Long id)
    {
        this.idcontext = id;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idcontext != null ? idcontext.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the idcontext fields are not set
        if (!(object instanceof ContextualDataBean))
        {
            return false;
        }
        ContextualDataBean other = (ContextualDataBean) object;
        if ((this.idcontext == null && other.idcontext != null) || (this.idcontext != null && !this.idcontext.equals(other.idcontext)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "prregister.beans.ContextualDataBean[ id=" + idcontext + " ]";
    }

    public LocalDate getEnrolmentdate()
    {
        return enrolmentdate;
    }

    public void setEnrolmentdate(LocalDate enrolmentdate)
    {
        this.enrolmentdate = enrolmentdate;
    }

    public IdentityBean getIdentityBean()
    {
        return identityBean;
    }

    public void setIdentityBean(IdentityBean identityBean)
    {
        this.identityBean = identityBean;
    }
    
    
    
}
