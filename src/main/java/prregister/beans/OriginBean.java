/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.beans;

import java.io.Serializable;
import javax.persistence.Embeddable;


@Embeddable
public class OriginBean implements Serializable
{
    private String province;
    private String territoire;
    private String village;

    public String getProvince()
    {
        return province;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getTerritoire()
    {
        return territoire;
    }

    public void setTerritoire(String territoire)
    {
        this.territoire = territoire;
    }

    public String getVillage()
    {
        return village;
    }

    public void setVillage(String village)
    {
        this.village = village;
    }
   
}
