/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.beans;

import java.io.Serializable;
import javax.persistence.Embeddable;


@Embeddable
public class AddressBean implements Serializable 
{
    private String numero;
    private String avenue;
    private String quartier;
    private String commune;

    public String getNumero()
    {
        return numero;
    }

    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    public String getAvenue()
    {
        return avenue;
    }

    public void setAvenue(String avenue)
    {
        this.avenue = avenue;
    }

    public String getQuartier()
    {
        return quartier;
    }

    public void setQuartier(String quartier)
    {
        this.quartier = quartier;
    }

    public String getCommune()
    {
        return commune;
    }

    public void setCommune(String commune)
    {
        this.commune = commune;
    }

    @Override
    public String toString()
    {
        return this.avenue+" "+"n°"+this.numero+", quartier "+this.quartier+", commune de: "+this.commune;
    }
    
}
