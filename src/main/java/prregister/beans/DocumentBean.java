/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import prregister.enumerations.DocType;

/**
 *
 * @author Fiston Songa
 */
@Entity
@Table(name = "document")
public class DocumentBean implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    private String documentID;
    
    @Enumerated(EnumType.STRING)
    private DocType docType;
    
    private int instance;
    
    @ManyToOne
    private IdentityBean identityBean;
    
    @OneToMany
    @JoinColumn(name = "idDocPart")
    private List<DocumentPartBean> documentParts;

    

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (documentID != null ? documentID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentBean))
        {
            return false;
        }
        DocumentBean other = (DocumentBean) object;
        if ((this.documentID == null && other.documentID != null) || (this.documentID != null && !this.documentID.equals(other.documentID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "prregister.beans.DocumentBean[ id=" + documentID + " ]";
    }

    public int getInstance()
    {
        return instance;
    }

    public void setInstance(int instance)
    {
        this.instance = instance;
    }

    public String getDocumentID()
    {
        return documentID;
    }

    public void setDocumentID(String documentID)
    {
        this.documentID = documentID;
    }

    public DocType getDocType()
    {
        return docType;
    }

    public void setDocType(DocType docType)
    {
        this.docType = docType;
    }

    public IdentityBean getIdentityBean()
    {
        return identityBean;
    }

    public void setIdentityBean(IdentityBean identityBean)
    {
        this.identityBean = identityBean;
    }

    public List<DocumentPartBean> getDocumentParts()
    {
        return documentParts;
    }

    public void setDocumentParts(List<DocumentPartBean> documentParts)
    {
        this.documentParts = documentParts;
    }
    
    
    
}
