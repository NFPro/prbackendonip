
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.beans;


import java.io.Serializable;
import java.util.List;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import prregister.enumerations.PersonStatus;
import prregister.enumerations.PhysicalStatus;


@Entity
@Table(name = "person")
public class PersonBean implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    private String personID;
    
    @Enumerated(EnumType.STRING)
    private PersonStatus status; 
    
    @Enumerated(EnumType.STRING)
    private PhysicalStatus physicalStatus;
    
    @OneToMany(mappedBy = "person")
    private List<IdentityBean> identities;
    

    public PersonStatus getStatus()
    {
        return status;
    }

    public void setStatus(PersonStatus status)
    {
        this.status = status;
    }

    public PhysicalStatus getPhysicalStatus()
    {
        return physicalStatus;
    }

    public void setPhysicalStatus(PhysicalStatus physicalStatus)
    {
        this.physicalStatus = physicalStatus;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (personID != null ? personID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonBean))
        {
            return false;
        }
        PersonBean other = (PersonBean) object;
        if ((this.personID == null && other.personID != null) || (this.personID != null && !this.personID.equals(other.personID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "onip.entities.Person[ id=" + personID + " ]";
    }

    public String getPersonID()
    {
        return personID;
    }

    public void setPersonID(String personID)
    {
        this.personID = personID;
    }

    public List<IdentityBean> getIdentities()
    {
        return identities;
    }

    public void setIdentities(List<IdentityBean> identities)
    {
        this.identities = identities;
    }
    
    

}
