/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prregister.beans;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Fiston Songa
 */
@Entity
@Table(name = "documentpart")
public class DocumentPartBean implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idDocPart;
    
    private int[] pages;
    private byte[] data;
    private String dataref;
    private int width;
    private int height;
    private LocalDate captureDate;
    private String capturedevice;
    private String format;
    
    @ManyToOne
     private DocumentBean documentBean;
    
    
    
    

    public Long getIdDocPart()
    {
        return idDocPart;
    }

    public void setIdDocPart(Long id)
    {
        this.idDocPart = id;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (idDocPart != null ? idDocPart.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the idDocPart fields are not set
        if (!(object instanceof DocumentPartBean))
        {
            return false;
        }
        DocumentPartBean other = (DocumentPartBean) object;
        if ((this.idDocPart == null && other.idDocPart != null) || (this.idDocPart != null && !this.idDocPart.equals(other.idDocPart)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "prregister.beans.DocumentPartBean[ id=" + idDocPart + " ]";
    }

    public LocalDate getCaptureDate()
    {
        return captureDate;
    }

    public void setCaptureDate(LocalDate captureDate)
    {
        this.captureDate = captureDate;
    }

    public String getCapturedevice()
    {
        return capturedevice;
    }

    public void setCapturedevice(String capturedevice)
    {
        this.capturedevice = capturedevice;
    }

    public String getFormat()
    {
        return format;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }

    public int[] getPages()
    {
        return pages;
    }

    public void setPages(int[] pages)
    {
        this.pages = pages;
    }

    public byte[] getData()
    {
        return data;
    }

    public void setData(byte[] data)
    {
        this.data = data;
    }

    public String getDataref()
    {
        return dataref;
    }

    public void setDataref(String dataref)
    {
        this.dataref = dataref;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public DocumentBean getDocumentBean()
    {
        return documentBean;
    }

    public void setDocumentBean(DocumentBean documentBean)
    {
        this.documentBean = documentBean;
    }
    
    
    
    
}
